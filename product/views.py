
# Create your views here.
from rest_framework import viewsets, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .models import Category, Product
from .serializers import CategorySerializer, ProductSerializer


class CategoryViewSet(viewsets.ModelViewSet):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()


class ProductViewSet(viewsets.ModelViewSet):
    serializer_class = ProductSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        try:
            return Product.objects.filter(company=self.request.user.company)
        except AttributeError:
            return Product.objects.filter(company=self.request.user.employee.company)

    def create(self, request, *args, **kwargs):
        company = request.user.company
        category = Category.objects.get(id=request.data.pop('category'))
        product = Product.objects.create(company=company, category=category, **request.data)
        return Response(data=self.serializer_class(product).data, status=status.HTTP_201_CREATED)

    def partial_update(self, request, *args, **kwargs):
        product = self.get_object()
        product.update_stock(stock=request.data['stock'])
        return Response(data=self.serializer_class(product).data)
