from django.db import models

# Create your models here.
from django.db.models import DO_NOTHING
from django.utils.translation import gettext_lazy as _
from djmoney.models.fields import MoneyField

from company.models import Company


class Category(models.Model):
    name = models.CharField(max_length=20)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name


class Product(models.Model):
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    name = models.CharField(_('product name'), max_length=20)
    description = models.TextField(null=True, blank=True)
    stock = models.IntegerField()
    category = models.ForeignKey(Category, on_delete=DO_NOTHING)
    price = MoneyField(max_digits=6, decimal_places=2, null=True, blank=True)

    def __str__(self):
        return self.name

    def update_stock(self, stock=0):
        self.stock = stock
        self.save()
