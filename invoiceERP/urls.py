"""invoiceERP URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework.routers import SimpleRouter
from company.views import CompanyViewSet
from customer.views import CustomerViewSet
from employee.views import EmployeeViewSet
from invoice.views import InvoiceViewSet
from product.views import CategoryViewSet, ProductViewSet
from user.views import UserViewSet

view_sets = [CompanyViewSet, UserViewSet, CategoryViewSet, ProductViewSet, CustomerViewSet, InvoiceViewSet,
             EmployeeViewSet]
API_ROUTE = 'api/v1/'
router = SimpleRouter()
for view in view_sets:
    name = view.__name__.replace('ViewSet', '').lower()
    router.register(name, view, basename=name)

urlpatterns = [
    path('admin/', admin.site.urls),
    path(API_ROUTE, include(router.urls))
]
