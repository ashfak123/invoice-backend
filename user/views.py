# Create your views here.
from django.contrib.auth import authenticate
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from .models import User
from .serializer import UserSerializer


class UserViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()

    def create(self, request, *args, **kwargs):
        user = User.objects.create_user(**request.data)
        return Response(UserSerializer(user).data, status=status.HTTP_201_CREATED)

    @action(methods=['POST'], detail=False)
    def get_access_token(self, request):
        user = authenticate(**request.data)
        if user:
            return Response(UserSerializer(user).data)
        else:
            return 'No user'

