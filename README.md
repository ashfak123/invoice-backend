**invoiceERP**

This project is a simplified sub module of ERP software we see in the market. So what we can do in this project are
*  Create a company profile with email and password
*  Add **Product**, **Customer** (with email), **Employee**(is a user), and company can create and send email **invoice** to customers
*  Company can assign permission to Employees when creating.
*  Employee can only access module(Product, customer, and invoice) if they have permissions.

Common package and service used in this projects are.
*  API authentication for REST framework - [simple JWT](https://github.com/davesque/django-rest-framework-simplejwt)
*  Generate invoice PDF - [Weasyprint](https://weasyprint.readthedocs.io/en/latest/)
*  serving E-mail to customer mail - [sendGrid](https://sendgrid.com/)

The permission for employee are created by assigning a [custom permission to the REST API](https://www.django-rest-framework.org/api-guide/permissions/#custom-permissions) viewset files , you can see the permission
 in employee/permission.py file.
 
 Here is the [API documentation link](https://www.getpostman.com/collections/0851ff006ad11f4242a8) 