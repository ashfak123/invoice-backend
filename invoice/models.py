from django.db import models

# Create your models here.
from django.utils.translation import ugettext_lazy as _
from djmoney.models.fields import MoneyField

from company.models import Company
from customer.models import Customer
from product.models import Product


class Invoice(models.Model):
    number = models.CharField(_('Invoice number'), max_length=20)
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    customer = models.ForeignKey(Customer, on_delete=models.DO_NOTHING)
    date = models.DateField()
    due_date = models.DateField(null=True, blank=True)
    total_amount = MoneyField(currency_max_length=10, max_digits=7, decimal_places=2, default=0)

    def __str__(self):
        return self.number


class InvoiceProduct(models.Model):
    invoice = models.ForeignKey(Invoice, on_delete=models.CASCADE, related_name='item_bought')
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    description = models.CharField(max_length=20)
    quantity = models.IntegerField()
    price = MoneyField(currency_max_length=10, max_digits=7, decimal_places=2)
    total_amount = MoneyField(currency_max_length=10, max_digits=7, decimal_places=2)

    def __str__(self):
        return self.product.name + str(self.quantity)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        # product = Product.objects.get(id=self.product)
        self.total_amount = self.quantity * self.price
        if not self.description:
            self.description = self.product.description
        print(self.invoice.id)
        super(InvoiceProduct, self).save(force_insert, force_update)
        self.invoice.total_amount += self.total_amount

