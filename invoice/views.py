from rest_framework import viewsets, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from customer.models import Customer
from employee.permissions import UserHasPermission
from product.models import Product
from utils.send_invoice import send_invoice
from .models import Invoice, InvoiceProduct
from .serializers import InvoiceSerializer


class InvoiceViewSet(viewsets.ModelViewSet):
    serializer_class = InvoiceSerializer
    permission_classes = [IsAuthenticated, UserHasPermission]

    def get_queryset(self):
        try:
            return Invoice.objects.filter(company=self.request.user.company)
        except AttributeError:
            return Invoice.objects.filter(company=self.request.user.employee.company)

    def create(self, request, *args, **kwargs):
        products = request.data.pop('products')
        amount = 0
        customer = Customer.objects.get(id=request.data.pop('customer'))
        invoice = Invoice.objects.create(company=request.user.company, customer=customer, **request.data)
        for item in products:
            product = Product.objects.get(id=item.pop('product'))
            invoice_product = InvoiceProduct.objects.create(invoice=invoice, product=product, **item)
            amount += invoice_product.total_amount
        invoice.total_amount = amount
        invoice.save()
        send_invoice(id=invoice.id)
        return Response(data=self.serializer_class(invoice).data, status=status.HTTP_201_CREATED)
