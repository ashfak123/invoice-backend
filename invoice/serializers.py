from rest_framework import serializers

from .models import Invoice, InvoiceProduct


class InvoiceProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = InvoiceProduct
        fields = '__all__'


class InvoiceSerializer(serializers.ModelSerializer):
    products = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Invoice
        fields = '__all__'

    def get_products(self, obj):
        return InvoiceProductSerializer(obj.item_bought.all(), many=True).data
