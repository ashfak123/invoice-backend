from django.contrib import admin

# Register your models here.
from .models import Invoice, InvoiceProduct

admin.site.register(Invoice)
admin.site.register(InvoiceProduct)
