# Create your models here.
from django.utils.translation import ugettext_lazy as _
from phonenumber_field.modelfields import PhoneNumberField
from django.contrib.auth import get_user_model
from django.db import models

user = get_user_model()


class Company(models.Model):
    user = models.OneToOneField(user, on_delete=models.CASCADE)
    name = models.CharField(_('Company name'), max_length=20)
    phone = PhoneNumberField(_('Mobile number'))
    address = models.TextField()
    district = models.CharField(_('District'), max_length=20)

    def __str__(self):
        return self.name
