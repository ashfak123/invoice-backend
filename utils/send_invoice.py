from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from weasyprint import HTML

from invoice.models import Invoice


def send_invoice(id=0):
    invoice = Invoice.objects.get(id=id)
    products = invoice.item_bought.all()
    file = render_to_string(template_name='index.html', context={"invoice": invoice, "products": products})
    h = HTML(string=file).write_pdf('demo.pdf')
    message = EmailMessage("Invoice",
                           "Hi {customer} \nHope this email finds you well! Please see attached invoice  for details."\
                           "Feel free to reach out if you have any questions.\nThank you for your business Regards "
                           "\n{company}".format(customer=invoice.customer, company=invoice.company),
                           invoice.company.user.email,
                           [invoice.customer.email, ],
                           )
    message.attach_file('demo.pdf')
    message.send()
