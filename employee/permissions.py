from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from rest_framework import permissions


class UserHasPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        try:
            if request.user.company:
                return True
        except AttributeError:
            if request.user.employee:
                model = view.serializer_class.Meta.model
                content_type = ContentType.objects.get_for_model(model)
                permission = Permission.objects.filter(content_type=content_type).values()
                permissions_list = [content_type.app_label+'.'+p['codename'] for p in permission]
                if request.user.has_perms(permissions_list):
                    return True
                return False
