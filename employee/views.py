# Create your views here.
from rest_framework import viewsets, status, exceptions
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from django.contrib.auth.models import Group
from .models import Employee
from .serializers import EmployeeSerializer
from user.models import User


class EmployeeViewSet(viewsets.ModelViewSet):
    serializer_class = EmployeeSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        try:
            return Employee.objects.filter(company=self.request.user.company)
        except AttributeError:
            raise exceptions.PermissionDenied()

    def create(self, request, *args, **kwargs):
        user_data = request.data.pop('user')
        user = User.objects.create_user(**user_data)
        group = Group.objects.get(id=request.data.pop('group'))
        group.user_set.add(user)
        employee = Employee.objects.create(user=user, company=request.user.company, **request.data)
        return Response(data=self.serializer_class(employee).data, status=status.HTTP_201_CREATED)
