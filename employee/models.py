from django.contrib.auth import get_user_model
from django.db import models

# Create your models here.
from company.models import Company
user = get_user_model()


class Employee(models.Model):
    name = models.CharField(max_length=20)
    user = models.OneToOneField(user, on_delete=models.CASCADE)
    company = models.ForeignKey(Company, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
