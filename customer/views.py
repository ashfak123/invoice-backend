# Create your views here.
from rest_framework import viewsets, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from customer.models import Customer
from customer.serializers import CustomerSerializer
from employee.permissions import UserHasPermission


class CustomerViewSet(viewsets.ModelViewSet):
    serializer_class = CustomerSerializer
    permission_classes = [IsAuthenticated, UserHasPermission]

    def get_queryset(self):
        try:
            return Customer.objects.filter(owner=self.request.user.company)
        except AttributeError:
            return Customer.objects.filter(owner=self.request.user.employee.company)

    def create(self, request, *args, **kwargs):
        company = request.user.company
        customer = Customer.objects.create(owner=company, **request.data)
        return Response(data=self.serializer_class(customer).data, status=status.HTTP_201_CREATED)
