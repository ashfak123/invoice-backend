# Create your models here.
from django.utils.translation import ugettext_lazy as _
from phonenumber_field.modelfields import PhoneNumberField
from django.db import models
from company.models import Company


class Customer(models.Model):
    owner = models.ForeignKey(Company, on_delete=models.CASCADE)
    name = models.CharField(_('customer name'), max_length=20)
    company = models.CharField(_('Company name'), max_length=20)
    phone = PhoneNumberField(_('Contact number'))
    email = models.EmailField(_('Email address'))
    address = models.TextField()
    district = models.CharField(max_length=20)

    def __str__(self):
        return self.name
